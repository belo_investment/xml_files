import json,xmltodict
import glob
from pandas.io.json import json_normalize
import pandas as pd
from bs4 import BeautifulSoup
#####################################
#####################################
#       Treating XML files         #
#####################################
xml_files = glob.glob("xml_files"+"/*.xml")
for i in range (len(xml_files)):
    with open(xml_files[i]) as xml_file:
        print(i)
        try:
            data_dict = xmltodict.parse(xml_file.read())
        except:
            print('ERROR >>> FILE ID',i,' NAME >',xml_files[i])
        
        xml_file.close()
    json_data = json.dumps(data_dict)
    #Create json file 
    with open(xml_files[i]+".json", "w") as json_file:
            json_file.write(json_data)
            json_file.close()
#####################################
#####################################
#       Treating JSON files         #
#####################################
global Lock 
Lock = 0
json_files = glob.glob("xml_files"+"/*.json")
for i in range (len(json_files)):
    print(i,'---',json_files[i])
    df = pd.read_json (json_files[i])
    df=  df.reset_index()

    #first block
    try:
        b = df.procEventoNFe[2]
        c = b['infEvento']
        if Lock==0:
        #CREATE empty dataframe once
            Lock=1
            print(Lock)
            data = pd.DataFrame (columns = c.keys()).reset_index()
        data = data.append(c,ignore_index=True)

        a = df.procEventoNFe[3]
        d = a['infEvento']
        data = data.append(d,ignore_index=True)
        data2 = data.append(data)
    except:
        print('ERROR >>> FILE ID',i,' NAME >',json_files[i])

data=data2.append(data)
data.to_excel('data.xlsx')



|field Name| Description|
|-------------|----------------|
|File_name| nome do arquivo|
|Emit_Cnpj| CNPJ do emitente do documento fiscal referenciado|
|Emit_Nome| Razão Social ou Nome do emitente|
|Emit_Bairro| Bairro|
|Emit_CEP| CEP|
|Dest_Cnpj| CNPJ do destinatário  |
|Dest_Nome| Razão Social ou nome do destinatário|
 |Dest_adr|  Dados do endereço|
 |Dest_CEP| CEP|
 |Dest_fone| Telefone preencher com Código DDD + número do telefone|
 |Entrega_Cnpj| CNPJ Autorizado|
|Entrega_adr| Endereço completo|
|Entrega_Bairro|Bairro|
 |Entrega_City| Cidade|
 |Entrega_UF| Código da UF|
 |cProd| Código do produto ou serviço. Preencher com CFOP caso se trate de itens não relacionados com mercadorias/produto e que o contribuinte não possua codificação própria|
|cEAN| GTIN (Global Trade Item Number) do produto antigo código EAN ou código de barras|
 |xProd| Descrição do produto ou serviço|
 |NCM| Código NCM (8 posições) será permitida a informação do gênero (posição do capítulo do NCM) quando a operação não for de comércio exterior (importação/exportação) ou o produto não seja tributado pelo IPI. Em caso de item de serviço ou item que não tenham produto (Ex. transferência de crédito crédito do ativo imobilizado etc.) informar o código 00 (zeros) (v2.0)|
 |CFOP| Cfop|
 |uCom| Unidade comercial|
|vUnCom| Valor unitário de comercialização  - alterado para aceitar 0 a 10 casas decimais e 11 inteiros|
|cEANTrib| GTIN (Global Trade Item Number) da unidade tributável antigo código EAN ou código de barras|
|qTrib| Quantidade Tributável - alterado para aceitar de 0 a 4 casas decimais e 11 inteiros|
|vUnTrib| Valor unitário de tributação - - alterado para aceitar 0 a 10 casas decimais e 11 inteiros|
|indTot| Este campo deverá ser preenchido com: 0 – o valor do item (vProd) não compõe o valor total da NF-e (vProd) 1  – o valor do item (vProd) compõe o valor total da NF-e|
|xPed| Informação do pedido|
|nItemPed| Número do Item do Pedido de Compra - Identificação do número do item do pedido de Compra|
 |Imposto_ICMS_orig| origem da mercadoria: 0 - Nacional 1 - Estrangeira -Importação direta 2 - Estrangeira - Adquirida no mercado interno|
|Imposto_ICMS_CST| Tributção pelo ICMS00 - Tributada integralmente|
 |Imposto_ICMS_vBC| Valor da BC do ICMS|
 |Imposto_ICMS_pICMS| Alíquota do ICMS|
 |Imposto_ICMS_vICMS| Valor do ICMS|
|Imposto_IPI_CST| Código da Situação Tributária do IPI:01-Entrada tributada com alíquota zero,02-Entrada isenta,03-Entrada não-tributada,04-Entrada imune,05-Entrada com suspensão,51-Saída tributada com alíquota zero,52-Saída isenta,53-Saída não-tributada,54-Saída imune,55-Saída com suspensão|
|Imposto_IPI_vBC| Base da BC do Imposto de Importação|
|Imposto_IPI_pPIP| Alíquota do IPI|
 |Imposto_IPI_vPIP| Valor do IPI|
 |Imposto_II_vBC| Base da BC do Imposto de Importação|
 |Imposto_vII| Valor do Imposto de Importação|
 |Imposto_II_vIOP| Valor do Imposto sobre Operações Financeiras|
 |Imposto_PIS_CST| Código de Situação Tributária do PIS. 01 – Operação Tributável - Base de Cálculo = Valor da Operação Alíquota Normal (Cumulativo/Não Cumulativo)-- 02 - Operação Tributável - Base de Calculo = Valor da Operação (Alíquota Diferenciada)|
 |Imposto_PIS_pPIS| Alíquota do PIS (em percentual)|
|Imposto_PIS_vPIS| Valor do PIS|
|Imposto_COFINS_CST| Código de Situação Tributária do COFINS.01 –Operação Tributável - Base de Cálculo = Valor da Operação Alíquota Normal (Cumulativo/Não Cumulativo) -- 02 - Operação Tributável - Base de Calculo = Valor da Operação (Alíquota Diferenciada)|
|Imposto_COFINS_vBC| Valor da BC do COFINS|
 |Imposto_COFINS_pCOFINS| Alíquota do COFINS (em percentual)|
 |Imposto_COFINS_vCOFINS| Valor do COFINS|
|Transporta_CNPJ| CNPJ do transportador|
|Transporta_Nome| Razão Social ou nome do transportador|
|Transporta_xEnder| Endereço completo|
|Transporta_xMun| Nome do munícipio|
|Transporta_UF| Sigla da UF|

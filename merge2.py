import json,xmltodict
import glob
from pandas.io.json import json_normalize
import pandas as pd
from bs4 import BeautifulSoup
import json
import ast
#####################################
#####################################
#       Treating XML files         #
#####################################
# def flatten_json(y):
#     out = {}

#     def flatten(x, name=''):
#         type(x)
#         if type(x) is dict:
#             for a in x:
#                 flatten(x[a], name + a + '_')
#         elif type(x) is list:
#             i = 0
#             for a in x:
#                 flatten(a, name + str(i) + '_')
#                 i += 1
#         elif type(x) is str:
#             x = ast.literal_eval(x[''])
#             for a in x:
#                 flatten(x[a], name + a + '_')
#         else:
#             out[name[:-1]] = x

#     flatten(y)
#     return out

xml_files = glob.glob("xml_files"+"/*.xml")
x=0
data_F=None
df_tags=None
Columns= ["File_name","Emit_Cnpj","Emit_Nome","Emit_Bairro","Emit_CEP",\
    "Dest_Cnpj","Dest_Nome","Dest_adr","Dest_CEP","Dest_fone",\
    "Entrega_Cnpj","Entrega_adr","Entrega_Bairro","Entrega_City","Entrega_UF",\
    "cProd","cEAN","xProd","NCM","CFOP","uCom","vUnCom","cEANTrib","qTrib","vUnTrib",\
    "indTot","xPed","nItemPed","Imposto_ICMS_orig","Imposto_ICMS_CST","Imposto_ICMS_vBC",\
    "Imposto_ICMS_pICMS","Imposto_ICMS_vICMS",\
    "Imposto_IPI_CST","Imposto_IPI_vBC","Imposto_IPI_pPIP","Imposto_IPI_vPIP",\
    "Imposto_II_vBC","Imposto_vII","Imposto_II_vIOP",\
    "Imposto_PIS_CST","Imposto_PIS_pPIS","Imposto_PIS_vPIS",\
    "Imposto_COFINS_CST","Imposto_COFINS_vBC","Imposto_COFINS_pCOFINS","Imposto_COFINS_vCOFINS",\
    "Transporta_CNPJ","Transporta_Nome","Transporta_xEnder","Transporta_xMun","Transporta_UF"]
list_tags_all=[]
data_F = pd.DataFrame(columns=Columns)
for i in range (len(xml_files)):
    with open(xml_files[i]) as xml_file:

        infile = open(xml_files[i],"r")
        contents = infile.read()
        soup = BeautifulSoup(contents,'xml')
        ####JSON
        # data_dict = xmltodict.parse(xml_file.read())
    # xml_file.close()
    # json_data = json.dumps(data_dict)
    # out = flatten_json(json_data)
        ########### Find all tag names
        try:

            list_tags = [tag.name for tag in soup.find_all()]
            list_tags_all = list_tags_all +  list_tags

        except:
            pass
            

        ###########
        det = soup.select("det")
        print(len(det))
        for j in range (len(det)):

            try:
                File_name =xml_files[j]
                emit = soup.select("emit")[j]
                Emit_Cnpj = emit.select("CNPJ")[j].text
                Emit_Nome = emit.select("xNome")[j].text
                Emit_Bairro = emit.select("xBairro")[j].text
                Emit_CEP = emit.select("CEP")[j].text
                
                
            except:
                
                Emit_Cnpj = None
                Emit_Nome = None
                Emit_Bairro = None
                Emit_CEP = None
                
            data=[File_name,Emit_Cnpj,Emit_Nome,Emit_Bairro,Emit_CEP]
            try:
                Dest = soup.select("dest")[j]
                Dest_Cnpj = Dest.select("CNPJ")[j].text
                Dest_Nome = Dest.select("xNome")[j].text
                Dest_adr = Dest.select("xLgr")[j].text
                Dest_CEP = Dest.select("CEP")[j].text
                Dest_fone = Dest.select("fone")[j].text
                
                
            except:
                
                Dest_Cnpj = None
                Dest_Nome = None
                Dest_adr = None
                Dest_CEP = None
                Dest_fone = None
                        
            data2 = [Dest_Cnpj,Dest_Nome,Dest_adr,Dest_CEP,Dest_fone]
            try:
                Entrega = soup.select("entrega")[j]
                Entrega_Cnpj = Entrega.select("CNPJ")[j].text
                Entrega_adr = Entrega.select("xLgr")[j].text
                Entrega_Bairro = Entrega.select("xBairro")[j].text
                Entrega_City = Entrega.select("xMun")[j].text
                Entrega_UF = Entrega.select("UF")[j].text
            
            except:
            
                Entrega_Cnpj = None
                Entrega_adr = None
                Entrega_Bairro = None
                Entrega_City = None
                Entrega_UF = None
            data3= [Entrega_Cnpj,Entrega_adr,Entrega_Bairro,Entrega_City,Entrega_UF]
            try:
                Prod = soup.select("prod")[j]
                cProd = Prod.select("cProd")[j].text
                cEAN =  Prod.select("cEAN")[j].text
                xProd = Prod.select("xProd")[j].text
                NCM = Prod.select("NCM")[j].text
                CFOP = Prod.select("CFOP")[j].text
                uCom = Prod.select("uCom")[j].text
                vUnCom = Prod.select("vUnCom")[j].text
                cEANTrib = Prod.select("cEANTrib")[j].text
                qTrib = Prod.select("qTrib")[j].text
                vUnTrib = Prod.select("vUnTrib")[j].text
                indTot =  Prod.select("indTot")[j].text
                xPed = Prod.select("xPed")[j].text
                nItemPed = Prod.select("nItemPed")[j].text
            except:
                
                cProd = None
                cEAN =  None
                xProd = None
                NCM = None
                CFOP = None
                uCom = None
                vUnCom = None
                cEANTrib = None
                qTrib = None
                vUnTrib = None
                indTot =  None
                xPed = None
                nItemPed = None
            data4= [cProd,cEAN,xProd,NCM,CFOP,uCom,vUnCom,cEANTrib,qTrib,vUnTrib,indTot,xPed,nItemPed]
            
            try:
                Imposto = soup.select("ICMS00")[j]
                Imposto_ICMS_orig = Imposto.select("orig")[j].text
                Imposto_ICMS_CST = Imposto.select("CST")[j].text
                Imposto_ICMS_vBC = Imposto.select("vBC")[j].text
                Imposto_ICMS_pICMS = Imposto.select("pICMS")[j].text
                Imposto_ICMS_vICMS = Imposto.select("vICMS")[j].text
            except:
                Imposto_ICMS_orig = None
                Imposto_ICMS_CST = None
                Imposto_ICMS_vBC = None
                Imposto_ICMS_pICMS = None
                Imposto_ICMS_vICMS = None
            data5= [Imposto_ICMS_orig,Imposto_ICMS_CST,Imposto_ICMS_vBC,Imposto_ICMS_pICMS,Imposto_ICMS_vICMS]
            
            try:
                Imposto_IPI = soup.select("IPITrib")[j]
                Imposto_IPI_CST = Imposto_IPI.select("CST")[j].text
                Imposto_IPI_vBC = Imposto_IPI.select("vBC")[j].text
                Imposto_IPI_pPIP = Imposto_IPI.select("pIPI")[j].text
                Imposto_IPI_vPIP = Imposto_IPI.select("vIPI")[j].text
                
            except:
                Imposto_IPI_CST = None
                Imposto_IPI_vBC = None
                Imposto_IPI_pPIP = None
                Imposto_IPI_vPIP = None
            data5_1 = [Imposto_IPI_CST,Imposto_IPI_vBC,Imposto_IPI_pPIP,Imposto_IPI_vPIP]             
            
            try:
                Imposto_II = soup.select("II")[j]
                Imposto_II_vBC = Imposto_II.select("vBC")[j].text
                Imposto_II_vII = Imposto_II.select("vII")[j].text
                Imposto_II_vIOP = Imposto_II.select("vIOF")[j].text

            except:
                Imposto_II_vBC = None
                Imposto_vII = None
                Imposto_II_vIOP = None
            data5_2=   [Imposto_II_vBC,Imposto_vII,Imposto_II_vIOP] 

            try:
                Imposto_PIS = soup.select("PISAliq")[j]
                Imposto_PIS_CST = Imposto_PIS.select("CST")[j].text
                Imposto_PIS_pPIS = Imposto_PIS.select("pPIS")[j].text
                Imposto_PIS_vPIS = Imposto_PIS.select("pPIS")[j].text
            except:
                Imposto_PIS_CST = None
                Imposto_PIS_pPIS = None
                Imposto_PIS_vPIS = None
            data5_3= [Imposto_PIS_CST,Imposto_PIS_pPIS,Imposto_PIS_vPIS] 

            try:
                Imposto_COFINS = soup.select("COFINSAliq")[j]
                Imposto_COFINS_CST = Imposto_COFINS.select("CST")[j].text
                Imposto_COFINS_vBC = Imposto_COFINS.select("vBC")[j].text
                Imposto_COFINS_pCOFINS = Imposto_COFINS.select("pCOFINS")[j].text
                Imposto_COFINS_vCOFINS = Imposto_COFINS.select("vCOFINS")[j].text

            except:
                Imposto_COFINS_CST = None
                Imposto_COFINS_vBC = None
                Imposto_COFINS_pCOFINS = None
                Imposto_COFINS_pCOFINS = None
            data5_4= [Imposto_COFINS_CST,Imposto_COFINS_vBC,Imposto_COFINS_pCOFINS,Imposto_COFINS_vCOFINS] 

            try:
                Transporta = soup.select("transporta")[j]
                Transporta_CNPJ = Transporta.select("CNPJ")[j].text
                Transporta_Nome = Transporta.select("xNome")[j].text
                Transporta_xEnder = Transporta.select("xEnder")[j].text
                Transporta_xMun = Transporta.select("xMun")[j].text
                Transporta_UF = Transporta.select("UF")[j].text
            except:
                Transporta_CNPJ = None
                Transporta_Nome = None
                Transporta_xEnder = None
                Transporta_xMun = None
                Transporta_UF = None
            data6= [Transporta_CNPJ,Transporta_Nome,Transporta_xEnder,Transporta_xMun,Transporta_UF]

    #Merging all data        
            try:
                data_all = [(data+data2+data3+data4+data5+data5_1+data5_2+data5_3+data5_4+data6)]
                dic = dict (zip(Columns,data_all[0]))
                if data_F is not None:
                    data_F= data_F.append(dic,ignore_index=True)
                    
                else:
                    data_F = pd.DataFrame(columns=Columns)
                
            except:
                print('Error forming dataframe')
dc  = list(dict.fromkeys(list_tags_all))
dc_dc= pd.DataFrame(data=dc,columns=['tags'])
#dc_dc.to_excel('all_tag_names.xls')
data_F.to_excel('data.xlsx')
